-- These may get overwritten by require, cache it now
local ModPath = ModPath
local ModInstance = ModInstance

if not package.loaded["jit.profile"] then
	package.loaded["jit.profile"] = package.preload["jit.profile"]()
end

local jit = assert(require("jit"))
jit.off()

local build_dir
local native_name

if blt.blt_info().platform == "mswindows" then
	build_dir = "profiler_native/cmake-build-release-visual-studio/"
	native_name = "profiler.dll"
else
	build_dir = "profiler_native/build/"
	native_name = "libprofiler.so"
end

local native_path = ModPath .. build_dir .. native_name
if not file.FileExists(native_path) then
	native_path = ModPath .. native_name
	assert(file.FileExists(native_path), "Neither debug native nor release native '" .. native_path .. "' exists")
end

local err, native = blt.load_native(native_path)
assert(native, err)

local prof = assert(require("jit.profile"))

prof.start("fi10", function(thread, samples, vmstate)
	local stack = prof.dumpstack(thread, "pFZ;;;;;;", -250)
	-- log(tostring(samples) .. " " .. vmstate .. " " .. stack)

	native.handle(samples, vmstate, stack)
end)

local function kb_dump()
	native.save()
	native.reset()
end

local function kb_reset()
	native.reset()
end

BLT.Keybinds:register_keybind(ModInstance, {
	id = "profiler_dump",
	allow_menu = true,
	allow_game = true,
	show_in_menu = nil, -- json_data["show_in_menu"],
	name = "Dump Profiler",
	desc = "Write out the profiling data",
	localize = false, -- TODO
	callback = kb_dump,
})

BLT.Keybinds:register_keybind(ModInstance, {
	id = "profiler_clear",
	allow_menu = true,
	allow_game = true,
	show_in_menu = nil,
	name = "Clear Profiler",
	desc = "Reset the profiling data",
	localize = false, -- TODO
	callback = kb_reset,
})
