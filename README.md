# Profiler Collector

When enabled it profiles the game, sampling the callstack every 10ms. It
defines a pair of keybinds, one writes the profile to a file in the root
PAYDAY 2 directory and clears the profiler, the other just clears the profiler.

This mod contains a native plugin, which patches LuaJIT to get the filenames
to come out correctly.

When you've saved your trace, feed it to the analyser: https://gitlab.com/SuperBLT/profiler/analyser

Source Code: https://gitlab.com/SuperBLT/profiler/collector

Licence: GNU General Public Licence, version 3 or later. See LICENCE.txt for more information.
