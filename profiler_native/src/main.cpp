// Include SuperBLT
#include <superblt_flat.h>

#include "myfuncs.h"

#include <assert.h>

#include <stdint.h>
#include <string.h>

#ifdef WIN32
// Windows version
#include <Windows.h>
#include <Psapi.h>

#else
// Linux version
#include <unistd.h>
#include <sys/mman.h>

#endif

#include <map>
#include <string>
#include <fstream>

#ifdef _MSC_VER

#include <stdlib.h>

#define bswap_32(x) _byteswap_ulong(x)
#define bswap_64(x) _byteswap_uint64(x)
#else

#include <byteswap.h>

#endif

// See superblt_flat.h for a description of what these functions do

#ifdef _MSC_VER

// Reimplement memmem, since it is unfortunately a GNU extension
static void *winmemmem(void *haystack, size_t haystack_len, void *needle, size_t needle_len) {
	auto *hs = (uint8_t *) haystack;
	auto *ne = (uint8_t *) needle;
	uint8_t start = ne[0];

	// Loop through each possible start location
	// We could use SSE or whatever to speed this up, but why bother?
	for (size_t i = 0; i < haystack_len - needle_len + 1; i++) {
		uint8_t *candidate = hs + i;
		if (*candidate != start) {
			continue;
		}

		if (memcmp(candidate, ne, needle_len) == 0) {
			return candidate;
		}
	}

	// Not found
	return nullptr;
}

static void patch_luajit() {
	MODULEINFO module;
	BOOL success = GetModuleInformation(GetCurrentProcess(), GetModuleHandle(nullptr), &module, sizeof(MODULEINFO));
	if (!success) {
		MessageBoxA(nullptr, "GetModuleInformation failed", "Profiler Patch Error", MB_OK);
		abort();
	}

	// The first 16 bytes of debug_putchunkname. This does not contain
	// any addresses so it should continue to work over PAYDAY 2 updates, and
	// there are no other occurrences of this in PD2.
	uint8_t signature[] = {
			0x8B, 0x44, 0x24, 0x08, 0x53, 0x83, 0x78, 0x2C,
			0xFF, 0x8B, 0x58, 0x28, 0x56, 0x57, 0x8D, 0x73
	};

	// The address of the debug_putchunkname function
	PD2HOOK_LOG_LOG("Searching for debug_putchunkname");
	auto funcname = (uint8_t *) winmemmem(module.lpBaseOfDll, module.SizeOfImage, signature, sizeof(signature));

	if (funcname == nullptr) {
		MessageBoxA(nullptr, "Could not find debug_putchunkname signature", "Profiler Patch Error", MB_OK);
		abort();
	}

	PD2HOOK_LOG_LOG("Searching complete, found debug_putchunkname");

	// This function will by default print out [string] for every
	// path that does not begin with @ or = so we need to patch it.
	// This is essentially what it does:
	//    cmp     al, '='                  // compare_addr is the pointer to this instruction
	//    jz      short process_filename
	//    cmp     al, '@'
	//    jz      short process_filename
	// We want to patch it as follows:
	//    test    al, al                   // Test al, setting the zero flag if null
	//    jnz     short process_filename   // jump to process_filename unless the zero flag is set
	//    cmp     al, '@'                  // unchanged, will never be true
	//    jz      short process_filename   // unchanged
	// This will make it print out the filenames of everything except
	// nulls.
	uint8_t *compare_addr = funcname + 0x9D;

	// Normally the first character in the filename is skipped. This is a pointer
	// to a "inc esi" instruction, with esi carrying the pointer to the filename. It
	// was used to skip over the filename, so replace it with a NOP
	uint8_t *start_pos = funcname + 0xE1;

	int res;
	DWORD old;
	res = VirtualProtect(compare_addr, (size_t) (start_pos - compare_addr + 1), PAGE_EXECUTE_READWRITE, &old);
	if (res == 0) {
		std::string msg = "VirtualProtect failed: " + std::to_string(GetLastError());
		MessageBoxA(nullptr, msg.c_str(), "Profiler Patch Error", MB_OK);
		abort();
	}

	// Change the comparison to "test al, al" since "cmp al, 0" is four bytes
	*compare_addr++ = 0x84;
	*compare_addr++ = 0xC0;

	// Change JZ to JNZ, since we want to jump if the string is empty
	*compare_addr++ = 0x75;

	// Thing increments a variable to skip the first char, make this a NOP
	*start_pos = 0x90;
}

#else

static void patch_luajit() {
	// The address of the debug_putchunkname function
	uint64_t funcname = 0x12FF460;

	// This function will by default print out [string] for every
	// path that does not begin with @ or = so we need to patch it.
	// It first compares the first character with '@', and if it does
	// not match it jumps. We patch it to check if the first character
	// is zero, and if so then jump.
	uint8_t *compare_addr = (uint8_t *) funcname + 0x13;

	// Normally the first character in the filename is skipped. This is the
	// address of the offset, which we decrease by one.
	uint64_t start_pos = funcname + 0x1F;

	int ps = getpagesize();
	uint64_t page_start = (uint64_t) compare_addr / ps * ps;

	int res;
	res = mprotect((void *) page_start, start_pos - page_start + 1, PROT_READ | PROT_EXEC | PROT_WRITE);
	assert(res == 0);

	// Change the comparison to "test al, al" since "cmp al, 0" is four bytes
	*compare_addr++ = 0x84;
	*compare_addr++ = 0xC0;

	// Change JNZ to JZ, since we want to jump if the string is empty
	*compare_addr++ = 0x74;

	// Start one char earlier
	(*(uint8_t *) start_pos)--;
}

#endif

void Plugin_Init() {
	PD2HOOK_LOG_LOG("Patching LuaJIT");
	patch_luajit();
}

void Plugin_Update() {
	// Put your per-frame update code here
}

void Plugin_Setup_Lua(lua_State *L) {
	// Deprecated, see this function's documentation (in superblt_flat.h) for more detail
}

class sample_data {
public:
	uint64_t self_hits;
	uint64_t stack_hits;
};

static std::map<std::string, sample_data> profiler_counter;

static int handle_log(lua_State *L) {
	int samples = lua_tointeger(L, 1);
	const char vmstate = *lua_tostring(L, 2);

	size_t len = 0;
	const char *stack = lua_tolstring(L, 3, &len);

	char *scratchpad = new char[len + 1];
	memcpy(scratchpad, stack, len);
	scratchpad[len] = 0;

	const char *split = ";;;;;;";
	size_t split_len = strlen(split);

	char *iter = scratchpad;
	char *partstart = scratchpad;

	char *end = iter + len - split_len;

	// PD2HOOK_LOG_LOG("Start");
	while (iter <= end) {
		// If this isn't a seperator, continue
		if (*iter != ';' || strncmp(iter, split, split_len) != 0) {
			iter++;
			continue;
		}

		*iter = 0;

		// Handle the chunk here
		// PD2HOOK_LOG_LOG(partstart);
		profiler_counter[partstart].stack_hits++;

		iter += split_len;
		partstart = iter;
	}

	if (partstart < scratchpad + len)
		profiler_counter[partstart].self_hits++;

	delete[] scratchpad;
	return 0;
}

static void write_u64(std::ofstream &s, uint64_t val) {
	uint64_t swapped = bswap_64(val);
	s.write((char *) &swapped, sizeof(swapped));
}

static int handle_save(lua_State *L) {
	PD2HOOK_LOG_LOG("Saving profiling data");

	std::ofstream out("profile.spd", std::ios::binary);

	const uint32_t version = bswap_32(1);
	out.write((char *) &version, sizeof(version));

	write_u64(out, profiler_counter.size());

	for (const auto &entry : profiler_counter) {
		const sample_data &d = entry.second;
		write_u64(out, d.self_hits);
		write_u64(out, d.stack_hits);

		const uint32_t size = bswap_32(entry.first.size());
		out.write((char *) &size, sizeof(size));

		out.write(entry.first.c_str(), entry.first.size());
	}

	return 0;
}

static int handle_reset(lua_State *L) {
	PD2HOOK_LOG_LOG("Clearing profiling data");
	profiler_counter.clear();
	return 0;
}

int Plugin_PushLua(lua_State *L) {
	// Create a Lua table
	lua_newtable(L);

	lua_pushcfunction(L, handle_log);
	lua_setfield(L, -2, "handle");

	lua_pushcfunction(L, handle_save);
	lua_setfield(L, -2, "save");

	lua_pushcfunction(L, handle_reset);
	lua_setfield(L, -2, "reset");

	// Now return the table to Lua
	return 1;
}
